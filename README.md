TICTACTOE GAME

This game is developed using PyQt5 programming. Please make sure you have the following pre-installed to run this file.

On Linux you can use the command to install:

sudo apt-get install python3-pyqt5

pip3 install emoji

Tips to start your game,

1. Two players namely, player x and player o are involved to play.

2. The first person who is gonna start the game will be player x and the second player will be player o.

3. Each player gets alternative chances to click.

4. The player who makes horizontal, vertical or diagonal pattern becomes a winner.

5. If both the players didn't get any chance to make patterns then the match becomes draw.