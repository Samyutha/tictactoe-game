import sys
import emoji
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QMainWindow, QLabel
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QRect

class App(QDialog):

    def __init__(self, MainWindow):
        super().__init__()
        self.title = 'TicTacToe Game'
        self.left = 350
        self.top = 50
        self.width = 300
        self.height = 180
        self.count = 0
        self.fill_list = []
        for i in range(10):
            self.fill_list.append(i)
        self.initUI(MainWindow)
        
    def initUI(self, MainWindow):
        self.MainWindow = MainWindow
        self.setStyleSheet("background-color: lightpink;") 
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.createGridLayout()
        windowLayout = QVBoxLayout()
        windowLayout.addWidget(self.horizontalGroupBox)
        self.setLayout(windowLayout)
        
        self.show()
    
    def createGridLayout(self):
        self.horizontalGroupBox = QGroupBox(emoji.emojize(':cyclone: Welcome to TicTacToe :cyclone:'))
        self.layout = QGridLayout()
        self.layout.setColumnStretch(2, 5)
        self.layout.setColumnStretch(3, 5)
        
        self.button1 = QPushButton('1')
        self.button1.setStyleSheet("background-color: lightgreen")
        self.button1.clicked.connect(self.on_click)
        self.layout.addWidget(self.button1,0,0)
        self.button2 = QPushButton('2')
        self.button2.setStyleSheet("background-color: lightgreen")
        self.button2.clicked.connect(self.on_click)
        self.layout.addWidget(self.button2,0,1)
        self.button3 = QPushButton('3')
        self.button3.setStyleSheet("background-color: lightgreen")
        self.button3.clicked.connect(self.on_click)
        self.layout.addWidget(self.button3,0,2)
        self.button4 = QPushButton('4')
        self.button4.setStyleSheet("background-color: lightgreen")
        self.button4.clicked.connect(self.on_click)
        self.layout.addWidget(self.button4,1,0)
        self.button5 = QPushButton('5')
        self.button5.setStyleSheet("background-color: lightgreen")
        self.button5.clicked.connect(self.on_click)
        self.layout.addWidget(self.button5,1,1)
        self.button6 = QPushButton('6')
        self.button6.setStyleSheet("background-color: lightgreen")
        self.button6.clicked.connect(self.on_click)
        self.layout.addWidget(self.button6,1,2)
        self.button7 = QPushButton('7')
        self.button7.setStyleSheet("background-color: lightgreen")
        self.button7.clicked.connect(self.on_click)
        self.layout.addWidget(self.button7,2,0)
        self.button8 = QPushButton('8')
        self.button8.setStyleSheet("background-color: lightgreen")
        self.button8.clicked.connect(self.on_click)
        self.layout.addWidget(self.button8,2,1)
        self.button9 = QPushButton('9')
        self.button9.setStyleSheet("background-color: lightgreen")
        self.button9.clicked.connect(self.on_click)
        self.layout.addWidget(self.button9,2,2)
        
        self.horizontalGroupBox.setLayout(self.layout)

    def on_click(self):
        if self.count % 2 == 0:
            write = 'x'
        else:
            write = 'o'
        sig_bt = self.MainWindow.sender()
        sig_text = sig_bt.text()
        if sig_text == "1":
            self.button1.hide()
            b1 = QPushButton(write)
            b1.setStyleSheet("background-color: purple")
            self.layout.addWidget(b1,0,0)
            self.fill_list[1] = write
        elif sig_text == "2":
            self.button2.hide()
            b2 = QPushButton(write)
            b2.setStyleSheet("background-color: purple")
            self.layout.addWidget(b2,0,1)
            self.fill_list[2] = write
        elif sig_text == "3":
            self.button3.hide()
            b3 = QPushButton(write)
            b3.setStyleSheet("background-color: purple")
            self.layout.addWidget(b3,0,2)
            self.fill_list[3] = write
        elif sig_text == "4":
            self.button4.hide()
            b4 = QPushButton(write)
            b4.setStyleSheet("background-color: purple")
            self.layout.addWidget(b4,1,0)
            self.fill_list[4] = write
        elif sig_text == "5":
            self.button5.hide()
            b5 = QPushButton(write)
            b5.setStyleSheet("background-color: purple")
            self.layout.addWidget(b5,1,1)
            self.fill_list[5] = write
        elif sig_text == "6":
            self.button6.hide()
            b6 = QPushButton(write)
            b6.setStyleSheet("background-color: purple")
            self.layout.addWidget(b6,1,2)
            self.fill_list[6] = write
        elif sig_text == "7":
            self.button7.hide()
            b7 = QPushButton(write)
            b7.setStyleSheet("background-color: purple")
            self.layout.addWidget(b7,2,0)
            self.fill_list[7] = write
        elif sig_text == "8":
            self.button8.hide()
            b8 = QPushButton(write)
            b8.setStyleSheet("background-color: purple")
            self.layout.addWidget(b8,2,1)
            self.fill_list[8] = write
        elif sig_text == "9":
            self.button9.hide()
            b9 = QPushButton(write)
            b9.setStyleSheet("background-color: purple")
            self.layout.addWidget(b9,2,2)
            self.fill_list[9] = write
        self.check_winner()
        self.count += 1
        if self.count == 9:
            self.label = QLabel(emoji.emojize('Game is draw :balloon:'),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 4, 1)

    def check_winner(self):
        if self.fill_list[1] == self.fill_list[2] == self.fill_list[3]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[1]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[4] == self.fill_list[5] == self.fill_list[6]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[4]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[7] == self.fill_list[8] == self.fill_list[9]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[7]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[1] == self.fill_list[4] == self.fill_list[7]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[1]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[2] == self.fill_list[5] == self.fill_list[8]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[2]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[3] == self.fill_list[6] == self.fill_list[9]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[3]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[3] == self.fill_list[5] == self.fill_list[7]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[3]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
            
        elif self.fill_list[1] == self.fill_list[5] == self.fill_list[9]:
            self.label = QLabel(emoji.emojize('{} is winner :thumbs_up:').format(self.fill_list[1]),self)
            self.label.adjustSize()
            self.layout.addWidget(self.label, 5, 1)
            self.label2 = QLabel(emoji.emojize(':confetti_ball::confetti_ball::confetti_ball::confetti_ball::confetti_ball:'),self)
            self.label2.adjustSize()
            self.layout.addWidget(self.label2, 6, 1)
        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    ex = App(MainWindow)
    sys.exit(app.exec_())
